function toggleBook(book) {
  var x = document.getElementById(book);

  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}