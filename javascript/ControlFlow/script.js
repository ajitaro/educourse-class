
// syntax pengulangan while
// var diulang = 0
// while (diulang < 10) {
//   console.log('Halo Bandung')
//   diulang = diulang + 1
// }

// syntax pengulangan for
// for (var i = 0; i < 5; i++) {
//   console.log('Halo Bandung')
// }

// SYNTAX KONDISIONAL

var ukuranBaju = prompt('Masukin ukuran baju kamu ...')
if ( ukuranBaju === 'S' || ukuranBaju === 'M') {
  alert('Harga baju S dan M = Rp 50.000')
} else if ( ukuranBaju === 'XL' ){
  alert('Harga baju XL = Rp 100.000')
} else {
  alert('Ukuran yang anda masukan salah')
}