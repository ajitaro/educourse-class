var menu = prompt(
  'Menu Rumah Makan Bahagia Makmur :\n' +
  '1. Nasi Goreng\n' +
  '2. Bakso\n' +
  '3. Mie Ayam\n' +
  '4. Es Cendol\n' +
  '5. Jus Alpukat\n' +
  '6. Kopi\n' +
  'Pilih Menu di atas dan masukkan angkanya :'
  )

switch( menu ) {
  case '1':
  case '2':
  case '3':
    alert('Makanan Anda akan segera kami antarkan')
    break
  case '4':
  case '5':
  case '6':
    alert('Minuman Anda akan segera kami antarkan')
    break
  default:
    alert('Anda memasukkan angka yang salah')
    break
}