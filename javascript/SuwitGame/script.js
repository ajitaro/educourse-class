var game = true
var nyawa = 2

while (game == true) {
  //Pilihan Player
  var player = prompt('Pilih : kertas, gunting, batu')

  //Pilihan Computer
  var computer = Math.random()

  if (computer < 0.34) {
    computer = 'batu'
  } else if (computer >= 0.34 && computer < 0.67) {
    computer = 'gunting'
  } else {
    computer = 'kertas'
  }
  computer = 'kertas'

  //Rules
  var hasil = ''

  if (player == computer) {
    hasil = 'SERI!'
  } else if (player == 'kertas') {
    if (computer == 'gunting') {
      hasil = 'KALAH!'
    } else {
      hasil = 'MEANANG!'
    }
  } else if (player == 'gunting') {
    if (computer == 'kertas') {
      hasil = 'MENANG!'
    } else {
      hasil = 'KALAH!'
    }
  } else if (player == 'batu') {
    if (computer == 'kertas') {
      hasil ='KALAH!'
    } else {
      hasil = 'MENANG!'
    }
  } else {
    hasil = ' memasukan pilihan yang salah!!!'
  }

  if (hasil == 'KALAH!') {
    nyawa--
  }

  //Hasilnya ditampilin
  alert('Kamu memilih : ' + player + ' dan Komputer memilih : ' + computer + '\nMaka hasilnya : Kamu ' + hasil + '\nNyawa kamu tinggal : ' + nyawa)
  if (nyawa == 0) {
    alert('GAME OVER!!!!!')
    game = confirm('Mau main lagi ?')

    nyawa = 2
  }
}

alert('Terima kasih sudah bermain')